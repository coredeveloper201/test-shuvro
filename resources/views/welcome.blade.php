@extends('layouts.app')

@section('content')
    <script src="https://cdn.auth0.com/js/auth0/9.0.0/auth0.min.js"></script>
    <script type="text/javascript">
        var webAuth = new auth0.WebAuth({
            domain: 'test-shuvro.auth0.com',
            clientID: 'YTIKh5UO2vjIzAT_FxHueI2UaEMyrx8L'
        });

        function signin() {
            webAuth.authorize({
                responseType: "code",
                redirectUri: 'http://localhost:8000/auth0/callback'
            });
        }
    </script>
    <button onclick="window.signin();">Login</button>
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ route('logout') }}">Logout</a>
            @else
                <a href="{{ route('login') }}">Login/Signup</a>
            @endauth
        </div>
    @endif
@endsection